import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'
import locale from 'element-ui/lib/locale'
import zhCNLocale from 'element-ui/lib/locale/lang/zh-CN'
import zhENLocale from 'element-ui/lib/locale/lang/en'
import zhCN from './zh-CN' 
import en from './en'


Vue.use(VueI18n)

export const messages = {
  'zh-CN': {
    '_lang': '简体中文',
    ...zhCN,
    ...zhCNLocale
  }, 
  'en': {
    '_lang': 'English',
    ...en,
    ...zhENLocale
  }
}

export default new VueI18n({
  locale: Cookies.get('language') || 'zh-CN',
  messages
})
